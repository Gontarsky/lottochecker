package GUI;

import DBService.DataSource;
import ServicePage.*;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static GUI.ConstantValues.*;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class SimpleGUI {
    private static int choice = 10;
    private static HtmlPageLotto htmlPageLotto;
    private final static String webLottoPl = "http://www.lotto.pl";
    private final static String webMegaLottoPl = "http://www.megalotto.pl";
    private static CssQueryLottoNumbers queryLottoNumbers = new CssQueryLottoNumbers("div");
    private static CssQueryLottoDate queryDate = new CssQueryLottoDate("div");
    private static CssQueryMegaLottoNumbers queryMegaLottoNumbers = new CssQueryMegaLottoNumbers("span");
    private static CssQueryMegaLottoDate queryMegaDate = new CssQueryMegaLottoDate("span");
    private static DataSource dataSource = new DataSource();

    public static void main(String args[]) {


        updateDatebase();

        while (choice != FIRST_QUIT) {
            show(CHOSENTIP + "");
            if (isChoiceChangedOrDamaged()) {

                switch (choice) {
                    case LOTTOPL: {
                        loadServiceMenu(webLottoPl, MENUSERVICES, queryLottoNumbers, queryDate);
                    }
                    break;

                    case MEGALOTTOPL: {
                        loadServiceMenu(webMegaLottoPl, MENUSERVICESML, queryMegaLottoNumbers, queryMegaDate);
                    }
                    break;
                    case CHECKNUMBERS: {
                        checkNumbersLotto(enterNumbersLotto(6), webMegaLottoPl);
                    }
                    break;
                    case QUANITYNUMBERS: {
                        checkQuanityOfNumbers();
                    }
                    break;
                    case FIRST_QUIT: {
                        choice = FIRST_QUIT;
                    }
                    break;
                }

                if (choice != FIRST_QUIT) {
                    showln(BLINK);
                    showln(MENUWHICHLOTTO);
                }
            }
        }
    }

    private static void updateDatebase() {
        showln(INFOABOUTPROGRAM + "\n" + INFOBEFORELOAD);
        if (dataSource.open())
        {
            if(dataSource.loadNumbersAndDatesToDatebaseFromURL
                (webMegaLottoPl, queryMegaLottoNumbers.getCssQueryNumbersLotto(), queryMegaDate.getCssQueryDateLottoAndPlus(), queryMegaDate.getTag())) {
                showln(INFOAFTERLOADOK);
            }
            else
                showln(INFOAFTERLOADERR);
        }

        show("\n" + CHOSESERVICE +
                "\n" + MENUWHICHLOTTO + "\n");
    }

    private static ArrayList<Integer> convertStringFromDBToArraylistInteger(String numbers) {
        ArrayList<String> templist = new ArrayList<String>((Arrays.asList(numbers.split(","))));
        ArrayList<Integer> numbersList = new ArrayList<>(templist.size());


        templist.forEach(temp -> numbersList.add(Integer.parseInt(temp)));

        return numbersList;
    }

    private static String convertArraylistIntegerToStringDB(ArrayList<Integer> numbers) {
        StringBuilder stringBuilder = new StringBuilder();
        numbers.forEach(number -> stringBuilder.append(number).append(","));

        return stringBuilder.toString();
    }

    private static void checkNumbersLotto(ArrayList<Integer> yourNumbers, String website) {
            try {
                htmlPageLotto = new HtmlPageLotto(website);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            if(htmlPageLotto != null) {
                if (htmlPageLotto.connect()) {
                    String date = htmlPageLotto.getDate(queryMegaDate.getCssQueryDateLottoAndPlus());
                    ArrayList<Integer> realNumbers = htmlPageLotto.getNumbers(
                            queryMegaLottoNumbers.getCssQueryNumbersLotto(), queryMegaLottoNumbers.getTag());
                    checkNumbersAndDate(yourNumbers, realNumbers, date);
                    checkHistoryOfNumbers(yourNumbers);
                } else
                    checkFromDB(yourNumbers);
            }
            else
                checkFromDB(yourNumbers);
    }

    private static void checkFromDB(ArrayList<Integer> yourNumbers) {
        System.out.println("Błąd związany z połączeniem internetowym. Sprawdź czy masz dostęp do internetu.");
        System.out.println(BLINK + "\n" + "Następuje sprawdzenie z ostatnim losowaniem z bazy danych:");
        if(dataSource.open()) {
            Statement st = dataSource.createStatement();
            if(st != null)
            {
                try {
                    ResultSet resultSet = st.executeQuery(dataSource.QUERY_FOR_ALL_RECORDS);
                    int countOfRecords = resultSet.getInt(1);

                    dataSource.queryDateForID.setInt(1,countOfRecords);
                    resultSet = dataSource.queryDateForID.executeQuery();
                    String data = resultSet.getString(1);

                    dataSource.queryNumbersForID.setInt(1,countOfRecords);
                    resultSet = dataSource.queryNumbersForID.executeQuery();
                    ArrayList<Integer> realNumbers = convertStringFromDBToArraylistInteger(resultSet.getString(1));

                    checkNumbersAndDate(yourNumbers,realNumbers,data );
                } catch (SQLException e) {
                    System.out.println("Błąd związany z błędnym zapytaniem: " + e.getMessage());
                }

            }
        }
    }

    private static void checkNumbersAndDate(ArrayList<Integer> yourNumbers,ArrayList<Integer> realNumbers, String date) {
        System.out.println("Data losowania: " + date);
        System.out.println("Liczby z losowania: " + realNumbers.toString());

        int counterOfNumbers = (int) realNumbers.stream().filter(realNumber ->
                yourNumbers.stream().anyMatch(yourNumber -> yourNumber.equals(realNumber))).count();

        System.out.println("Trafiłeś " + counterOfNumbers + "/6");
    }

    private static void checkHistoryOfNumbers(ArrayList<Integer> numbers) {
        try {
            if (dataSource.open()) {
                dataSource.queryNumbersSerach.setString(1, convertArraylistIntegerToStringDB(numbers));
                ResultSet results = dataSource.queryNumbersSerach.executeQuery();
                ArrayList<String> dates = new ArrayList<>();

                while (results.next()) {
                    dates.add(results.getString(dataSource.COLUMN_RESULTS_DATE));
                }

                for (String temp : dates)
                    System.out.println("CIEKAWOSTKA:\n" + BLINK + "\nMając takie numery trafiłbyś 6 dnia " + temp + "\n" + BLINK);
                dataSource.close();
            }
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
        }
    }

    private static void checkQuanityOfNumbers() {
        try {
            if (dataSource.open()) {
                ResultSet results = dataSource.queryQuantityNumbers.executeQuery();
                ArrayList<ArrayList<Integer>> listNumbers = new ArrayList<>();

                while (results.next()) {
                    listNumbers.add(convertStringFromDBToArraylistInteger(results.getString(dataSource.COLUMN_RESULTS_NUMBERS)));
                }

                int counter;
                ArrayList<Integer> quanity = new ArrayList<>();

                System.out.println("Podaj liczbę ostatnich losowań z których chcesz zobaczyć statystyki ( max " + listNumbers.size() + ") : ");
                int numberOfLastDraws = new Scanner(System.in).nextInt();

                System.out.println("Podaj liczbę ostatnich losowań których nie uwzgledni program ( max " + listNumbers.size() + ") : ");
                int numberOfLastWithoutDraws = new Scanner(System.in).nextInt();

                for (int i = 1; i <= 49; i++) {
                    counter = 0;
                    for (int j = listNumbers.size() - numberOfLastDraws; j < listNumbers.size() - numberOfLastWithoutDraws; j++) {
                        if (listNumbers.get(j).contains(i))
                            counter++;
                    }
                    quanity.add(counter);
                }



                int temp = 1;
                for (Integer numb : quanity) {
                    System.out.println(temp++ + " - " + numb);
                }

                dataSource.close();
            }
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
        }
    }

    private static void loadServiceMenu(String website, String menu, ICssQueryForNumbers iCssQueryForNumbers, ICssQueryForDate iCssQueryForDate) {
        try {
            htmlPageLotto = new HtmlPageLotto(website);
            if (htmlPageLotto.connect()) {
                while (choice != SECOND_QUIT) {
                    show("\n" + BLINK +
                            "\n" + menu + "\n" + BLINK + "\n");
                    show(CHOSENTIP);
                    if (isChoiceChangedOrDamaged())
                        showServiceLotto(choice, iCssQueryForNumbers, iCssQueryForDate);
                }
            } else
                System.out.println("Błąd nawiązywania połączenia z " + website + "\nKod błędu: " + htmlPageLotto.getStatusSite());
        } catch (UnknownHostException | SocketTimeoutException sockEx) {
            System.out.println("Upłynął czas połączenia ze stroną. Sprawdź swoje połączenie internetowe i spróbuj za chwilę.\nBłąd związany nawiązaniem połączenia ze stroną: " + sockEx.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void showServiceLotto(int newChoice, ICssQueryForNumbers iCssQueryForNumbers, ICssQueryForDate iCssQueryForDate) {
        switch (newChoice) {
            case NUMLOTTO: {
                System.out.println(htmlPageLotto.getDate(iCssQueryForDate.getCssQueryDateLottoAndPlus()));
                htmlPageLotto.show(htmlPageLotto.getNumbers(iCssQueryForNumbers.getCssQueryNumbersLotto(), iCssQueryForNumbers.getTag()));
                showMenuReturn();
            }
            break;
            case NUMLOTTOPLUS: {
                System.out.println(htmlPageLotto.getDate(iCssQueryForDate.getCssQueryDateLottoAndPlus()));
                htmlPageLotto.show(htmlPageLotto.getNumbers(iCssQueryForNumbers.getCssQueryNumbersLottoPlus(), iCssQueryForNumbers.getTag()));
                showMenuReturn();
            }
            break;
            case NUMMULTIMULTI: {
                System.out.println(htmlPageLotto.getDate(iCssQueryForDate.getCssQueryDateMultiMulti()));
                htmlPageLotto.show(htmlPageLotto.getNumbersWithPlus(iCssQueryForNumbers.getCssQueryNumbersMultiMulti(), iCssQueryForNumbers.getTag(), iCssQueryForNumbers.getCssQueryNumberMultiMultiPlus()));
                showMenuReturn();
            }
            break;
            case NUMMINILOTTO: {
                System.out.println(htmlPageLotto.getDate(iCssQueryForDate.getCssQueryDateMiniLotto()));
                htmlPageLotto.show(htmlPageLotto.getNumbers(iCssQueryForNumbers.getCssQueryNumbersMiniLotto(), iCssQueryForNumbers.getTag()));
                showMenuReturn();
            }
            break;
            case NUMEXTRACASH: {
                System.out.println(htmlPageLotto.getDate(iCssQueryForDate.getCssQueryDateExtraCash()));
                htmlPageLotto.show(htmlPageLotto.getNumbers(iCssQueryForNumbers.getCssQueryNumbersExtraCash(), iCssQueryForNumbers.getTag()));
                showMenuReturn();
            }
            break;
        }
    }

    private static boolean isChoiceChangedOrDamaged() {
        Scanner scanner = new Scanner(System.in);

        try {
            choice = scanner.nextInt();
            return true;
        } catch (InputMismatchException inputMisExp) {
            System.out.println("Proszę podaj cyfrę wybóru: ");
            return false;
        }

    }

    private static void showMenuReturn() {
        showln(MENURETURN);
        while (new Scanner(System.in).nextInt() != RETURNTOMENU) {
            showln(MENURETURN);
        }
    }

    private static ArrayList<Integer> enterNumbersLotto(int maxNumbers) {
        Scanner scanner = new Scanner(System.in);
        showln("Podaj " + maxNumbers + " liczb z lotto (1-49): ");
        ArrayList<Integer> listOfYourNumbers = new ArrayList<>(6);
        String temp = "";
        while (listOfYourNumbers.size() < maxNumbers) {
            try {
                temp = scanner.nextLine();
                listOfYourNumbers.add(Integer.parseInt(temp));
            } catch (NumberFormatException e) {
                System.out.println("Błąd: Podano znak '" + temp + "' zamiast liczby.");
            }
        }
        return listOfYourNumbers;
    }

    private static void show(String text) {
        System.out.print(text);
    }

    private static void showln(String text) {
        System.out.println(text);
    }
}

