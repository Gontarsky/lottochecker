package GUI;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class ConstantValues {
    final static String INFOABOUTPROGRAM = "Lotto-Checker ver: 0.0.1\n";
    final static String CHOSENTIP = "Wybierz: ";
    final static String CHOSESERVICE = "Wybierz z którego serwisu chcesz zobaczyć numery: ";
    final static String MENUWHICHLOTTO = "1.www.lotto.pl\n2.www.megalotto.pl\n3.Wpisz swoje liczby i sprawdź w Lotto\n4.Jak często liczby występowały\n5.Wyjście";
    final static String MENUSERVICES = "LOTTO.PL\nMenu:\n1.Lotto\n2.Lotto-Plus\n3.Multi-Multi\n4.Mini-Lotto\n5.Ekstra-Pensja\n6.Wyjście";
    final static String MENUSERVICESML = "MEGALOTTO.PL\nMenu:\n1.Lotto\n2.Lotto-Plus\n3.Multi-Multi\n4.Mini-Lotto\n5.Ekstra-Pensja\n6.Wyjście";
    final static String BLINK = "------------------------------------------------------------------";
    final static String MENURETURN = "Menu:\n1.Powrót do wcześniejszego menu.";
    final static String INFOBEFORELOAD = "Trwa synchronizowanie bazy danych z wynikami...";
    final static String INFOAFTERLOADOK = "Zakończono pozytywnie zsynchronizowanie.";
    final static String INFOAFTERLOADERR = "Nie udało się zsynchronizować bazy danych.";
    final static int QUANITYNUMBERS = 4;
    final static int CHECKNUMBERS = 3;
    final static int RETURNTOMENU = 1;
    final static int NUMLOTTO = 1;
    final static int NUMLOTTOPLUS = 2;
    final static int NUMMULTIMULTI = 3;
    final static int NUMMINILOTTO = 4;
    final static int NUMEXTRACASH = 5;
    final static int LOTTOPL = 1;
    final static int MEGALOTTOPL = 2;
    final static int FIRST_QUIT = 5;
    final static int SECOND_QUIT = 6;
}
