package ServicePage;

import java.util.ArrayList;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public interface ICssQueryForNumbers {
    public String getCssQueryNumbersLotto();
    public String getCssQueryNumbersLottoPlus();
    public String getCssQueryNumbersExtraCash();
    public String getCssQueryNumbersMiniLotto();
    public String getCssQueryNumbersMultiMulti();
    public String getCssQueryNumberMultiMultiPlus();
    public String getTag();


}
