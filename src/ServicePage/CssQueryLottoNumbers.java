package ServicePage;

public class CssQueryLottoNumbers implements ICssQueryForNumbers {
    private String tag;

    public CssQueryLottoNumbers(String tag) {
        this.tag = tag;
    }

    public String getCssQueryNumbersLotto() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(1) > div.glowna_wyniki_lotto > div.wynik_lotto";
    }

    @Override
    public String getCssQueryNumbersLottoPlus() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(2) > div > div.wynik_lotto";
    }

    @Override
    public String getCssQueryNumbersExtraCash() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(5) > div.glowna_wyniki_ekstra-pensja > div.wynik_ekstra-pensja";
    }

    @Override
    public String getCssQueryNumbersMiniLotto() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(7) > div.glowna_wyniki_mini-lotto > div.wynik_mini-lotto";
    }

    @Override
    public String getCssQueryNumbersMultiMulti() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(9) > div.glowna_wyniki_multi-multi > div.wynik_multi-multi";
    }

    @Override
    public String getCssQueryNumberMultiMultiPlus() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(9) > div.wynik_multi-multi_plus";
    }

    @Override
    public String getTag() {
        return tag;
    }


}