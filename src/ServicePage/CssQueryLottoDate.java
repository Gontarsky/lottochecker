package ServicePage;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class CssQueryLottoDate implements ICssQueryForDate {
    public String tag;

    public CssQueryLottoDate(String tag) {
        this.tag = tag;
    }

    @Override
    public String getCssQueryDateLottoAndPlus() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(1) > div.wyniki_data";
    }

    @Override
    public String getCssQueryDateExtraCash() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(5) > div.wyniki_data";
    }

    @Override
    public String getCssQueryDateMiniLotto() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(7) > div.wyniki_data";
    }

    @Override
    public String getCssQueryDateMultiMulti() {
        return "#block-block_wyniki-0 > div.content > div.start-wyniki > div:nth-child(1) > div:nth-child(9) > div.wyniki_data";
    }


    @Override
    public String getTag() {
        return tag;
    }
}
