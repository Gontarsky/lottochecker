package ServicePage;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public interface ICssQueryForDate {
    public String getCssQueryDateLottoAndPlus();
    public String getCssQueryDateExtraCash();
    public String getCssQueryDateMiniLotto();
    public String getCssQueryDateMultiMulti();
    public String getTag();
}
