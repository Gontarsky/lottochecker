package ServicePage;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class HtmlPageLotto {


    private String nameOfWebsite;
    private Document webPage;
    private Connection.Response response;


    public HtmlPageLotto(String nameOfWebsite) throws IOException {
        this.nameOfWebsite = nameOfWebsite;
        response = Jsoup.connect(this.nameOfWebsite).timeout(10*1000).execute();

    }

    public void setNameOfWebsite(String nameOfWebsite) {
        this.nameOfWebsite = nameOfWebsite;
    }

    public int getStatusSite()
    {
        return response.statusCode();
    }

    public boolean connect()
    {
        int SUCCESS_REQUEST = 200;
        if(getStatusSite() == SUCCESS_REQUEST)
        {
            try {
                webPage = Jsoup.connect(nameOfWebsite).timeout(10*1000).get();
                return true;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return false;
    }

    public ArrayList<Integer> getNumbers(String cssQueryForNumbers,String tag)
    {
        ArrayList<Integer> numbersOfLotto = new ArrayList<>();
        Elements detailsOfPageNumbers = webPage.select(cssQueryForNumbers);

        detailsOfPageNumbers.forEach(detail ->
                numbersOfLotto.add(Integer.valueOf(detail.getElementsByTag(tag).first().text())));
        return numbersOfLotto;
    }

    public ArrayList<Integer> getNumbersWithPlus(String cssQueryForNumbers,String tag, String cssQueryForPlus)
    {
        ArrayList<Integer> numbersOfLotto = getNumbers(cssQueryForNumbers,tag);
        Elements detailOfPlus = webPage.select(cssQueryForPlus);

        detailOfPlus.forEach(detail -> numbersOfLotto.add(Integer.valueOf(
                detail.getElementsByTag(tag).first().text())));
        return numbersOfLotto;
    }

    public ArrayList<String> getNumbersForDatebase(String cssQueryForNumbers,String tag, int maxNumbers)
    {
        ArrayList<String> numbersOfLotto = new ArrayList<>();
        Elements detailsOfPageNumbers = webPage.select(cssQueryForNumbers);

        String temp;
        for(int i = 0; i < detailsOfPageNumbers.size();) {
            temp ="";
            for (int j = 0; j < maxNumbers && i < detailsOfPageNumbers.size(); j++) {
                temp += detailsOfPageNumbers.get(i).getElementsByTag(tag).first().text()+",";
                i++;
            }
            numbersOfLotto.add(temp);
        }
        return numbersOfLotto;
    }

    public String getNumbersForCheckIntoDatebase(String cssQueryForNumbers, String tag)
    {
        String numbersOfLotto = "";
        Elements detailsOfPageNumbers = webPage.select(cssQueryForNumbers);

        String temp;
        for(int i = 0; i < detailsOfPageNumbers.size();) {
                numbersOfLotto += detailsOfPageNumbers.get(i).getElementsByTag(tag).first().text()+",";
        }
        return numbersOfLotto;
    }

    public void show(ArrayList<Integer> numbers)
    {
         numbers.forEach(System.out::println);
    }

    public String getDate(String cssQueryForData)
    {
        return webPage.select(cssQueryForData).text();
    }

    public ArrayList<String> getDates(String cssQueryForNumbers,String tag)
    {
        ArrayList<String> datesOfLotto = new ArrayList<>();
        Elements detailsOfPageNumbers = webPage.select(cssQueryForNumbers);

        detailsOfPageNumbers.forEach(detail ->
                datesOfLotto.add(detail.getElementsByTag(tag).first().text().replace('.','-')));
        return datesOfLotto;
    }

}
