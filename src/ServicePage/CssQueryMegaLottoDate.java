package ServicePage;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class CssQueryMegaLottoDate implements ICssQueryForDate {
    private String tag;

    public CssQueryMegaLottoDate(String tag) {
        this.tag = tag;
    }

    @Override
    public String getCssQueryDateLottoAndPlus() {
        return "#wyniki_lotto_na_stronie_glownej_blink > h5";
    }

    @Override
    public String getCssQueryDateExtraCash() {
        return "#wyniki_ekstra_pensji_na_stronie_glownej_blink > h5";
    }

    @Override
    public String getCssQueryDateMiniLotto() {
        return "#wyniki_mini_lotto_na_stronie_glownej_blink > h5";
    }

    @Override
    public String getCssQueryDateMultiMulti() {
        return "#wyniki_multi_multi_na_stronie_glownej_blink > h5";
    }

    @Override
    public String getTag() {
        return tag;
    }
}
