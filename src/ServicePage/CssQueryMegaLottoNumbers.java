package ServicePage;

/**
 * Created by Liberator1907 on 14.09.2017.
 */
public class CssQueryMegaLottoNumbers implements ICssQueryForNumbers{
    private   String tag;

    public CssQueryMegaLottoNumbers(String tag) {
        this.tag = tag;
    }

    @Override
    public String getCssQueryNumbersLotto() {
        return "#wyniki_lotto_na_stronie_glownej_blink > ul > li";
    }

    @Override
    public String getCssQueryNumbersLottoPlus() {
        return "#wyniki_lotto_plus_na_stronie_glownej_blink > ul > li";
    }

    @Override
    public String getCssQueryNumbersExtraCash() {
        return "#wyniki_ekstra_pensji_na_stronie_glownej_blink > ul > li";
    }

    @Override
    public String getCssQueryNumbersMiniLotto() {
        return "#wyniki_mini_lotto_na_stronie_glownej_blink > ul > li";
    }

    @Override
    public String getCssQueryNumbersMultiMulti() {
        return "#wyniki_multi_multi_na_stronie_glownej_blink > ul:nth-child(3) > li";
    }

    @Override
    public String getCssQueryNumberMultiMultiPlus() {
        return "#wyniki_multi_multi_na_stronie_glownej_blink > ul:nth-child(4) > li";
    }

    @Override
    public String getTag() {
        return tag;
    }
}
