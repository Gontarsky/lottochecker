package DBService;


import ServicePage.*;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;


/**
 * Created by Liberator1907 on 15.09.2017.
 */
public class DataSource {
    private static final String DB_NAME = "DBResults.db";
    private static final String CONNECTION_STRING = "jdbc:sqlite:C:\\Users\\Liberator1907\\Documents\\ProjectsGIT\\FetchInfoFromNet\\"+DB_NAME;

    private static final String TABLE_RESULTS_LOTTO = "Results_Lotto";
    private static final String COLUMN_RESULTS_NR =  "Nr";
    public static final String COLUMN_RESULTS_DATE = "Date";
    public static final String COLUMN_RESULTS_NUMBERS = "Numbers";

    public static final int ID_COLUMN_RESULTS_DATE = 2;

    private static final String INSERT_NUMBERS = "INSERT INTO " + TABLE_RESULTS_LOTTO +
            '(' + COLUMN_RESULTS_DATE + ", " + COLUMN_RESULTS_NUMBERS + ") VALUES(?,?)";

    private static final String QUERY_RESULT = "SELECT " + COLUMN_RESULTS_NR + " FROM " +
            TABLE_RESULTS_LOTTO + " WHERE " + COLUMN_RESULTS_DATE + " = ? AND " + COLUMN_RESULTS_NUMBERS + " = ?";

    private static final String QUERY_DATES_FOR_NUMBERS = "SELECT " + COLUMN_RESULTS_DATE + " FROM " +
            TABLE_RESULTS_LOTTO + " WHERE " + COLUMN_RESULTS_NUMBERS + " = ?" ;

    private static final String QUERY_FOR_QUANTITY_OF_NUMBERS = "SELECT " + COLUMN_RESULTS_NUMBERS + " FROM " +
            TABLE_RESULTS_LOTTO;

    private static final String QUERY_FOR_DATES = "SELECT " + COLUMN_RESULTS_DATE + " FROM " +
            TABLE_RESULTS_LOTTO;

    private static final String QUERY_NUMBERS_FOR_ID = "SELECT " + COLUMN_RESULTS_NUMBERS +  " FROM " +
            TABLE_RESULTS_LOTTO + " WHERE " + COLUMN_RESULTS_NR + " = ?";
    private static final String QUERY_DATE_FOR_ID = "SELECT " + COLUMN_RESULTS_DATE + " FROM " +
            TABLE_RESULTS_LOTTO +  " WHERE " + COLUMN_RESULTS_NR + " = ?";

    public  final String QUERY_FOR_ALL_RECORDS = "SELECT COUNT(*) FROM " + TABLE_RESULTS_LOTTO;

    private PreparedStatement queryResults;
    private PreparedStatement insertIntoResults_Lotto;

    public PreparedStatement queryNumbersForID;
    public PreparedStatement queryDateForID;
    public PreparedStatement queryNumbersSerach;
    public PreparedStatement queryQuantityNumbers;
    private PreparedStatement queryDates;


    private Connection connection;

    public DataSource(){}

    public Statement createStatement()
    {
        try {
            return connection.createStatement();
        } catch (SQLException e) {
            System.out.println("Błąd związany z utworzeniem zapytania: " + e.getMessage());
        }
        return null;
    }

    public boolean open()
    {
        try{
            connection = DriverManager.getConnection(CONNECTION_STRING);
            queryResults = connection.prepareStatement(QUERY_RESULT);
            queryNumbersSerach = connection.prepareStatement(QUERY_DATES_FOR_NUMBERS);
            insertIntoResults_Lotto = connection.prepareStatement(INSERT_NUMBERS);
            queryQuantityNumbers = connection.prepareStatement(QUERY_FOR_QUANTITY_OF_NUMBERS);
            queryDates = connection.prepareStatement(QUERY_FOR_DATES);
            queryNumbersForID = connection.prepareStatement(QUERY_NUMBERS_FOR_ID);
            queryDateForID = connection.prepareStatement(QUERY_DATE_FOR_ID);
            return true;
        }catch(SQLException e)
        {
            System.out.println("Opening was failed : " + e.getMessage());
            return false;
        }
    }

    public void close()
    {
        try{
            if(insertIntoResults_Lotto != null)
            {
                insertIntoResults_Lotto.close();
            }
            if(queryNumbersSerach != null)
            {
                queryNumbersSerach.close();
            }
            if( queryQuantityNumbers != null)
            {
                queryQuantityNumbers.close();
            }
            if( queryDates != null)
            {
                queryDates.close();
            }
            if( queryResults != null)
            {
                queryResults.close();
            }
            if( queryNumbersForID != null)
            {
                queryNumbersForID.close();
            }
            if( queryDateForID != null)
            {
                queryDateForID.close();
            }
            if( connection != null)
            {
                connection.close();
            }
        }catch (SQLException e)
        {
            System.out.println("Couldn't close connection: "+e.getMessage());
        }
    }



    public int insertResult(String date,String numbers) throws SQLException {

        queryResults.setString(1, date);
        queryResults.setString(2, numbers);
        ResultSet results = queryResults.executeQuery();

        if (results.next()) {
            return results.getInt(1);
        } else {
            insertIntoResults_Lotto.setString(1, date);
            insertIntoResults_Lotto.setString(2, numbers);
            int affectedRows = insertIntoResults_Lotto.executeUpdate();

            if(affectedRows != 1)
                throw new SQLException("Nie można dodać numerów do bazy.!");

            ResultSet generatedKeys = insertIntoResults_Lotto.getGeneratedKeys();
            if(generatedKeys.next())
            {
                return generatedKeys.getInt(1);
            }
            else
            {
                throw new SQLException("Nie można dodać numerów do bazy.");
            }
        }
    }


    public boolean loadNumbersAndDatesToDatebaseFromURL(String nameOfWebsite, String queryForNumbers, String queryForDate, String tag)
    {
            try {
                HtmlPageLotto page = new HtmlPageLotto(nameOfWebsite);
                if(page.connect()) {
                    ArrayList<String> listNumbers = page.getNumbersForDatebase(queryForNumbers, tag, 6);
                    ArrayList<String> listDates = page.getDates(queryForDate, tag);

                    int counter_dates = listDates.size() - 1;
                    for (int i = listNumbers.size() - 1; i >= 0 && counter_dates >= 0; i--) {
                        this.insertResult(listDates.get(counter_dates), listNumbers.get(i));
                        counter_dates--;
                    }
                    this.close();
                    return true;
                }

            } catch (SQLException | IOException e) {
                System.out.println("Błąd połączenia ze stroną: " + e.getMessage());
                this.close();
                return false;
            }

        return false;
        }
}
